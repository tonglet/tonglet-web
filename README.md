## tonglet-web

Live @ [https://tonglet-project.web.app](https://tonglet-project.web.app/)

100% open source.

Feel free to contribute on this project.

## TODOs

### Top Priorty
- [x] Finish backend sync function
- [x] Begin UI Prototype
- [x] Implement UI
- [ ] Buy domain
- [ ] Meta tags

## Please Help
- [ ] Refractor Design
- [ ] Try another Theme UI color
- [ ] Cache data in local storage
- [ ] Responsive redesign with `xs` etc...
- [ ] Solve known issues
- [ ] API Security
- [ ] Need MUCH CODE Refactoring
- [ ] See more on issues...

## Todos and plans
- [ ] Test to redesign with ReactJS, Bootstrap, AJAX, jQuery
- [ ] Publish Android and iOS
