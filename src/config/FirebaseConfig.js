import * as firebase from 'firebase';
import {firebaseConfig} from '../constants/FirebaseConstant';

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

export default firebase;
/**
 * ES6 modules are singleton patterns.
 * Read more https://stackoverflow.com/questions/48492047/where-do-i-initialize-firebase-app-in-react-application#comment109875915_48495381
 */