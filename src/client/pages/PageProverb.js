import React from 'react';
import SentenceTranslationComponent from "../components/SentenceTranslationComponent";
export default function PageProverb(){
    return(
        <SentenceTranslationComponent
        title="Paunak le Akhiatna"
        docType="proverb"
      />
    );
}