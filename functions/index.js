/* eslint-disable consistent-return */
"use strict"; // to write cleaner code, like preventing you from using undeclared variables.

/**
 * Importing OS module
 */
const os = require("os");

/**
 * Importing Firebase library
 */
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
const firestore = admin.firestore();

/**
 * Getting secrets from Firebase Config
 */
const sheetConfig = functions.config().sheet;
const mongoConfig = functions.config().mongodb;
const allowedUserConfig = functions.config().alloweduser;

/**
 * Importing Google Spread Sheet library
 */
const { GoogleSpreadsheet } = require("google-spreadsheet");
const doc = new GoogleSpreadsheet(sheetConfig.spreadsheetid); // Calling with Spread Sheet ID
const bookDoc = new GoogleSpreadsheet(sheetConfig.bookspreadsheetid); // Calling with Spread Sheet ID

/**
 * Connecting to MongoDB service
 */
const MongoClient = require("mongodb").MongoClient;
const uri = `mongodb+srv://${mongoConfig.username}:${mongoConfig.key}@tonglet-cluster.gppcn.mongodb.net/`;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = async (req, res, next) => {
  console.log("Check if request is authorized with Firebase ID token");

  if (
    (!req.headers.authorization ||
      !req.headers.authorization.startsWith("Bearer ")) &&
    !(req.cookies && req.cookies.__session)
  ) {
    console.error(
      "No Firebase ID token was passed as a Bearer token in the Authorization header.",
      "Make sure you authorize your request by providing the following HTTP header:",
      "Authorization: Bearer <Firebase ID Token>",
      'or by passing a "__session" cookie.'
    );
    res.status(403).send("Unauthorized");
    return;
  }

  let idToken;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer ")
  ) {
    console.log('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split("Bearer ")[1];
  } else if (req.cookies) {
    console.log('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  } else {
    // No cookie
    res.status(403).send("Unauthorized");
    return;
  }

  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken);
    console.log("ID Token correctly decoded", decodedIdToken);
    req.user = decodedIdToken;
    
    // decline any other users
    if (
      decodedIdToken.email === allowedUserConfig.one ||
      decodedIdToken.email === allowedUserConfig.two ||
      decodedIdToken.email === allowedUserConfig.three
    ) {
      console.log("USER CORRECT!!!");
      next();
      return;
    }
    res.status(403).send("Unauthorized");
    return;
  } catch (error) {
    console.error("Error while verifying Firebase ID token:", error);
    res.status(403).send("Unauthorized");
    return;
  }
};

/**
 * Enabling CORS problems
 */
const cookieParser = require("cookie-parser")();
const express = require("express");
const cors = require("cors")({ origin: true });
const app = express();
const clientapi = express();
app.use(cors);
app.use(cookieParser);
app.use(validateFirebaseIdToken);
clientapi.use(cors);

/**
 *      ### getDocument ###
 * Get documents with `TYPE`
 */
clientapi.post("/getDocument", async (req, res) => {
  // Log
  functions.logger.log("function:getDocument - called.");

  const docType = req.body.data.docType;

  functions.logger.log("function:getDocument - request=>", docType);

  client.connect((err, db) => {
    if (err) {
      functions.logger.warn(
        "function:getDocument - mongodb client connection failed",
        err
      );
      return;
    }

    return db
      .db("tonglet-web")
      .collection("items")
      .find({ docType: docType })
      .toArray((error, result) => {
        if (error) {
          // Log unsuccessful
          functions.logger.warn("function:getDocument - error:", error);
          return res.status(204).json({
            data: error,
          });
        }
        // Log successful
        functions.logger.log("function:getDocument - successful:", result);
        return res.status(200).json({
          data: result,
        });
      });
  });
});

/**
 *        ### searchDocument ###
 * Search doucments by keywords
 */
clientapi.post("/searchDocument", async (req, res) => {
  // Log
  functions.logger.log("function:searchDocument - called.");

  const keywords = req.body.data.keywords;
  const key = req.body.data.key;
  const sensitivity = req.body.data.sensitivity;

  functions.logger.log(
    "function:searchDocument - request=>",
    key,
    keywords,
    sensitivity
  );

  client.connect((err, db) => {
    if (err) {
      functions.logger.warn(
        "function:searchDocument - mongodb client connection failed",
        err
      );
      return;
    }

    /**
     * A = Search in `origin`.
     *
     * Else, B = Search in `v2`. Grab its `origin` and Search by `origin`
     * Else, C = Search in `v3`. Grab its `origin` and Search by `origin`.
     * Else, D = Search in `ving`. Grab its `origin` and Search by `origin`.
     * Else, E = Search in `plural`. Grab its `origin` and Search by `origin`.
     *
     */

    // please refractor this if you can
    const query = {};
    sensitivity
      ? (query[`${key}`] = keywords)
      : (query[`${key}`] = new RegExp("^" + keywords, "i"));

    return db
      .db("tonglet-web")
      .collection("items")
      .find(query)
      .limit(10)
      .sort({ key: 1 })
      .toArray((error, result) => {
        if (error) {
          // Log unsuccessful
          functions.logger.warn("function:searchDocument - error:", error);
        }
        // Log successful
        functions.logger.log("function:searchDocument - successful:", result);
        return res.status(200).json({
          data: result,
        });
      });
  });
});

/**
 *      ### getDocumentCollection ###
 * get a collection of documents by `origin`
 */
clientapi.post("/getDocumentCollection", async (req, res) => {
  // Log
  functions.logger.log("function:getDocumentCollection - called.");

  const keywords = req.body.data.keywords;

  functions.logger.log("function:getDocumentCollection - request=>", keywords);

  client.connect((err, db) => {
    if (err) {
      functions.logger.warn(
        "function:getDocumentCollection - mongodb client connection failed",
        err
      );
      return;
    }

    let query = {};
    query.origin = new RegExp("^" + keywords + "$");
    return db
      .db("tonglet-web")
      .collection("items")
      .find(query)
      .toArray((error, result) => {
        if (error) {
          // Log unsuccessful
          functions.logger.warn(
            "function:getDocumentCollection - error:",
            error
          );
          return res.status(204).json({
            data: error,
          });
        }
        // Log successful
        functions.logger.log(
          "function:getDocumentCollection - successful:",
          result
        );
        return res.status(200).json({
          data: result,
        });
      });
  });
});

/**
 * get books uploaded by Users
 */
clientapi.post("/getBooks", async (req, res) => {
  // Log
  functions.logger.log("function:getBooks - called.");

  // Using service account credentials
  await bookDoc.useServiceAccountAuth({
    client_email: sheetConfig.client_email,
    private_key: sheetConfig.private_key,
  });

  // Initialisation required. Loads document properties and worksheets
  await bookDoc.loadInfo();

  // Calls by worksheet index. Index[0] is a sheet that contains metadata of other sheets
  const sheetIndex = await bookDoc.sheetsByIndex[0]; // by sheet index
  const rowsIndex = await sheetIndex.getRows();
  let rows = [];

  rowsIndex.forEach((row) => {
    // rowCounts is zero, exclude it.
    if(String(row['Okay to publish?']).toLowerCase().trim()==="yes"){
      let myrow = {
        name: row['Na Min (Name)'],
        title: row['Thulu (Title):'],
        description: row['Athu (About)'],
        textLink: row['Laibu/thuluii hiah guang in (Upload your file here)'], 
        mediaLink: row['Zola / Zo Video link te hiah guang in (Put your song and video link here)'],
        fileType: row['File Name (File Type)'],
      }
      rows.push(myrow);
    }
    
  });

  

  return res.status(200).json({
    "data" :{
      rows
    }
  });
});

/**
 *      ### ADMIN ###
 */

/**
 *      ### diff ###
 * This function returns the differren between
 */
app.post("/diff", async (req, res) => {
  // Log
  functions.logger.log("function:diff - called.");

  const bookRef = req.body.data.bookRef;
  const maxRow = Number(req.body.data.maxRow);

  functions.logger.log("function:diff - request=>", bookRef, maxRow);

  client.connect((err, db) => {
    if (err) {
      functions.logger.warn(
        "function:preInsert - mongodb client connection failed",
        err
      );
      return;
    }

    return db
      .db("tonglet-web")
      .collection("items")
      .countDocuments({ bookRef: bookRef })
      .then((result) => {
        functions.logger.log("function:diff - count:", result);
        return res.status(200).json({
          data: maxRow - result,
        });
      })
      .catch((error) => {
        functions.logger.warn("function:diff - error:", error);
        return res.status(204).json({
          data: error,
        });
      });
  });
});

/**
 *      ### getSheetInfo ###
 * This function returns { position, rowCounts }
 * @return {json} - returns a json object of positions and rowCounts
 */
app.post("/getSheetInfo", async (req, res) => {
  // Log
  functions.logger.log("function:getSheetInfo - called.");

  // Using service account credentials
  await doc.useServiceAccountAuth({
    client_email: sheetConfig.client_email,
    private_key: sheetConfig.private_key,
  });

  // Initialisation required. Loads document properties and worksheets
  await doc.loadInfo();

  // Calls by worksheet index. Index[0] is a sheet that contains metadata of other sheets
  const sheetIndex = await doc.sheetsByIndex[0]; // by sheet index
  const rowsIndex = await sheetIndex.getRows();
  let positions = [];
  let rowCounts = [];
  let bookRefs = [];

  rowsIndex.forEach((row) => {
    // rowCounts is zero, exclude it.
    if (Number(row.rowCount) !== 0) {
      bookRefs.push(row.bookRef);
      positions.push(row.position);
      rowCounts.push(row.rowCount);
    }
  });

  // Log
  functions.logger.log(
    "function:getSheetInfo -  result.",
    positions,
    rowCounts
  );

  res.status(200).json({
    data: {
      bookRefs: bookRefs,
      positions: positions,
      rowCounts: rowCounts,
    },
  });
});

/**
 *      ###  preInsert ###
 * This function prepares before inserting.
 * (1) It clears the existing old collection.
 * (2) It updates the system as maintainence status.
 */
app.post("/preInsert", async (req, res) => {
  // Log
  functions.logger.log("function:preInsert - called.");

  client.connect((err, db) => {
    if (err) {
      functions.logger.warn(
        "function:preInsert - mongodb client connection failed",
        err
      );
      return;
    }

    // Log
    functions.logger.log("function:preInsert - connected.");

    return Promise.all([
      // Clear tonglet-web.items collection.
      db.db("tonglet-web").collection("items").drop(),
      // Log
      functions.logger.log(
        "function:preInsert - tonglet-web.items collection emptied."
      ),
      // Set the system status as maintenance.
      firestore.collection("config").doc("system").update({ webLive: false }),
      // Log
      functions.logger.log(
        "function:preInsert - firestore.config.system as inactive."
      ),
    ])
      .then((result) => {
        functions.logger.log("items collection successful: ", result);

        // @todos - solve this ugly return
        return res.send({
          data: {
            status: true,
          },
        });
      })
      .catch((error) => {
        functions.logger.warn("items collection dropped: ", error);
        // @todos - solve this ugly return
        return res.send({
          data: {
            status: false,
          },
        });
      });
  });
});

/**
 * This function commits batch bulk insertion into MongoDB database.
 *
 * @param {number} req.body.startLoop - the begining row index of worksheet
 * @param {number} req.body.endLoop - the end row index of worksheet
 * @param {number} req.body.index - the worksheet index number
 *
 * @todo - remove NOT OPT row properties to non-optional in production
 */
app.post("/beginInsert", async (req, res) => {
  // Log
  functions.logger.log("function:beginInsert - called.");

  // Requested JSON Object
  const startLoop = Number(req.body.data.startLoop);
  const endLoop = Number(req.body.data.endLoop);
  const index = Number(req.body.data.index);

  // Log
  functions.logger.log(
    "function:beginInsert - request =>",
    startLoop,
    endLoop,
    index
  );

  // Using service account credential
  await doc.useServiceAccountAuth({
    client_email: sheetConfig.client_email,
    private_key: sheetConfig.private_key,
  });

  // Initialisation required. Loads document properties and worksheets
  await doc.loadInfo();
  // Selecting worksheet by index
  const sheet = doc.sheetsByIndex[index];
  // Getting rows of selected worksheet
  const rows = await sheet.getRows();

  client.connect((err, db) => {
    if (err) {
      functions.logger.warn(
        "function:beginInsert - client connection failed.",
        err
      );
      return res.send({
        data: {
          status: false,
        },
      });
    }

    // Log
    functions.logger.log("function:beginInsert - connected.");

    let documents = [];

    for (var i = startLoop; i < endLoop; i++) {
      const item = rows[i];
      // Log
      functions.logger.log("rows info: ", item);
      const row = {
        bookRef: item.bookRef,
        docType: item.docType,
        fromLang: item.fromLang,
        toLang: item.toLang,
        mainWord: String(item.mainWord).toLowerCase().trim(), // NOT OPT
        origin:
          item.hasOwnProperty("origin") && item.origin !== undefined
            ? String(item.origin).toLowerCase().trim()
            : "", // optional
        wordType: item.hasOwnProperty("wordType")
          ? String(item.wordType).trim()
          : "", // optional
        definitionArray:
          item.hasOwnProperty("definitionArray") &&
          item.definitionArray !== undefined
            ? String(item.definitionArray).split("\n")
            : [], // NOT OPT
        field:
          item.hasOwnProperty("field") && item.field !== undefined
            ? String(item.field).trim()
            : "", // optional
        credit:
          item.hasOwnProperty("credit") && item.credit !== undefined
            ? String(item.credit).trim()
            : "", // optional
        pluralNoun:
          item.hasOwnProperty("pluralNoun") && item.pluralNoun !== undefined
            ? String(item.pluralNoun).toLowerCase().trim()
            : "", // optional
        v2Verb:
          item.hasOwnProperty("v2Verb") && item.v2Verb !== undefined
            ? String(item.v2Verb).toLowerCase().trim()
            : "", // optional
        v3Verb:
          item.hasOwnProperty("v3Verb") && item.v3Verb !== undefined
            ? String(item.v3Verb).toLowerCase().trim()
            : "", // optional
        vingVerb:
          item.hasOwnProperty("vingVerb") && item.vingVerb !== undefined
            ? String(item.vingVerb).toLowerCase().trim()
            : "", // optional
        synonymArray:
          item.hasOwnProperty("synonymArray") && item.synonymArray !== undefined
            ? String(item.synonymArray).split("\n")
            : [], // optional
      };

      documents.push({ insertOne: { document: row } });
    }

    return Promise.all([
      /**
       * @todo - detect unsuccessful document write operation
       */
      db.db("tonglet-web").collection("items").bulkWrite(documents),
    ])
      .then((result) => {
        // Log
        functions.logger.log("items collection imported: ", result);

        // @todo - ugly return
        return res.send({
          data: {
            status: true,
          },
        });
      })
      .catch((error) => {
        // Log
        functions.logger.warn("items collection unsuccessful: ", error);
        // @todo - ugly return
        return res.send({
          data: {
            status: false,
            error: error,
          },
        });
      });
  });
});

/**
 * This function create MongoDB datbase index for searching performance.
 * @todo Implement the idea.
 */
app.post("/postInsert", async (req, res) => {
  // Log
  functions.logger.log("function:postInsert - called.");

  client.connect((err, db) => {
    if (err) {
      functions.logger.warn("postInsert failed", err);
      return;
    }

    // Log
    functions.logger.log("function:postInsert - connected.");

    return Promise.all([
      //@todo add create index mongodb here
      // Set the system status as active.
      firestore.collection("config").doc("system").update({ webLive: true }),
    ])
      .then((result) => {
        functions.logger.log("items collection successful: ", result);

        // @todo - ugly return
        return res.send({
          data: {
            status: true,
          },
        });
      })
      .catch((error) => {
        functions.logger.warn("items collection dropped: ", error);
        // @todo - ugly return
        return res.send({
          data: {
            status: false,
          },
        });
      });
  });
});

os.freemem();

const runtimeOpts = {
  timeoutSeconds: 300,
  memory: "2GB",
};

exports.app = functions.runWith(runtimeOpts).https.onRequest(app);
exports.clientapi = functions.runWith(runtimeOpts).https.onRequest(clientapi);
